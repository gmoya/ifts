<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materia extends Model

{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'id','codigo','nombre','descripcion','anio','comision','carrera_id','docente_id'
    ];

    protected $table = 'materia';
    
    /**
     * Get the Docente that owns the Materia.
     */
    public function docente()
    {
        return $this->belongsTo(Docente::class);
    }

    /**
     * Get the Carrera that owns the Materia.
     */
    public function carrera()
    {
        return $this->belongsTo(Carrera::class);
    }
}
