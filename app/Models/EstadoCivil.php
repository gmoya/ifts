<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    protected $table= 'estado_civil';

	public function persona()
   	{
		return $this->hasMany(Persona::class);
   	}
	   
	public function __toString()
	{
		return $this->descripcion;
	}
}
