<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoSexo extends Model
{
	protected $table= 'tipo_sexo';

    public function persona()
   	{
		return $this->hasMany(Persona::class);
   	}

	public function __toString()
	{
		return $this->descripcion;
	}
}
