<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class inscripcion_materia extends Model
{
    use HasFactory;
    protected $fillable = [
      'materia_id','alumno_id','carrera_id',
      ];
      protected $table = 'inscripcion_materia';
}
