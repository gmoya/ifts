<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    //@TODO-CAMBIAR A USUARIO LOGUEADO
    protected $attributes=['created_by_id' => 1];
	
    protected $fillable = [
        'id', 'nombre', 'apellido', 'tipo_documento_id', 'nro_documento', 'sexo_id', 'celular', 'email', 'nacionalidad_id', 'estado_civil_id', 'fecha_nacimiento', 'lugar_nacimiento', 'observaciones', 'user_cl_id', 'created_at', 'created_by_id', 'updated_at', 'updated_by_id', 'deleted_at', 'deleted_by_id'
    ];

    protected $table = 'persona';

    /**
     * Get the Docente associated with the Persona.
     */
    public function docente()
    {
        return $this->hasOne(Docente::class);
    }

    public function tipo_documento()
    {
        return $this->belongsTo(TipoDocumento::class);
    }
    
    public function nacionalidad()
    {
        return $this->belongsTo(Nacionalidad::class);
    }

    public function sexo()
    {
        return $this->belongsTo(TipoSexo::class);
    }
    public function estado_civil()
    {
        return $this->belongsTo(EstadoCivil::class);
    }
}
