<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    use HasFactory;
    //@TODO-CAMBIAR A USUARIO LOGUEADO
    protected $attributes=['created_by_id'=>1];
    protected $fillable = [
        'id','nombre','descripcion','estado','institucion','observaciones','tipo_carrera_id',
'tipo_formacion_id','modalidad_dictado_id','condicion_ingreso_id','created_at','created_by_id',
'updated_at','updated_by_id','deleted_at','deleted_by_id'
    ];

    protected $table = 'carrera';
}




