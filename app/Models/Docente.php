<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    use HasFactory;

    protected $attributes=['created_by_id'=>1];
    protected $fillable = [
        'id', 'legajo', 'codigo', 'estado', 'persona_id', 'created_at', 'created_by_id', 'updated_at', 'updated_by_id', 'deleted_at', 'deleted_by_id'
    ];

    protected $table = 'docente';

    /**
     * Get the Persona that owns the Docente.
     */
    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

    public function getNombreCompleto()
    {
        return $this->persona->nombre. ' ' .$this->persona->apellido;
    }
}
