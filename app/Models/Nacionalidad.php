<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nacionalidad extends Model
{
    protected $table= 'nacionalidad';

    public function persona()
   	{
		return $this->hasMany(Persona::class);
   	}
	   
	public function __toString()
	{
		return $this->nombre;
	}
}