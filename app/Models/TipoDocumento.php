<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{	
	protected $table= 'tipo_documento';

	public function persona()
   	{
		return $this->hasMany(Persona::class);
   	}

	public function __toString()
	{
		return $this->descripcion;
	}
}
