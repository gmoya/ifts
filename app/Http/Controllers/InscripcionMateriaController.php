<?php

namespace App\Http\Controllers;

use App\Models\inscripcion_materia;
use App\Models\Materia;
use App\Models\Carrera;
use Illuminate\Http\Request;

class InscripcionMateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = inscripcion_materia::latest()->paginate(5);
    
      return view('inscripcion_materia.index',compact('data'))
          ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response**
     */
    public function create()
    {
    $inscripcion_materia=inscripcion_materia::all();
    $carrera=Carrera::all();
    $materia=Materia::all();

        return view('inscripcion_materia.create',compact('inscripcion_materia','materia','carrera'))
        
        
;
        
    }

    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
      $request->validate([
        // 'materia_id' => 'required',
    ]);

    // Materia::create($request->all());
    // Carrera::create($request->all());
    inscripcion_materia::create($request->all());
    return redirect()->route('inscripcion_materia.index')
                    ->with('success','inscripción a materia creada satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\inscripcion_materia  $inscripcion_materia
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
      
      $inscripcion_materia=inscripcion_materia::findOrfail($id);
        return view('inscripcion_materia.show',compact('inscripcion_materia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\inscripcion_materia  $inscripcion_materia
     * @return \Illuminate\Http\Response
     */
    public function edit($id) { 
      
      $inscripcion_materia=inscripcion_materia::findOrfail($id);
      return view('inscripcion_materia.edit',compact('inscripcion_materia'));
}
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\inscripcion_materia  $inscripcion_materia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

      $inscripcion_materia = request()->except(['_token','_method']);
      inscripcion_materia::where('id','=',$id)->update($inscripcion_materia);

      $inscripcion_materia=inscripcion_materia::findOrfail($id);
      return redirect()->route('inscripcion_materia.index')
      ->with('success','Inscripción actualizada satisfactoriamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\inscripcion_materia  $inscripcion_materia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      inscripcion_materia::destroy($id);

      return redirect()->route('inscripcion_materia.index')
                      ->with('success','Inscripcion eliminada satisfactoriamente');
    }
}
