<?php

namespace App\Http\Controllers;

use App\Models\Carrera;
use App\Models\Docente;
use Illuminate\Http\Request;
use App\Models\Materia;

class MateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Materia::paginate(20);
    
        return view('materia.index',compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $carreras = Carrera::all();
        $docentes = Docente::all();

        return view('materia.create', compact('carreras', 'docentes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'codigo' => 'required',
        'nombre' => 'required',
    ]);

    Materia::create($request->all());

    return redirect()->route('materia.index')
                    ->with('success','materia creada satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
      
      $materia=Materia::findOrfail($id);
        return view('materia.show',compact('materia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $materia = Materia::findOrfail($id);
        $carreras = Carrera::all();
        $docentes = Docente::all();

        return view('materia.edit', compact('materia', 'carreras', 'docentes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

      $datosMateria = request()->except(['_token','_method']);
      Materia::where('id','=',$id)->update($datosMateria);

      $materia=Materia::findOrfail($id);
      return redirect()->route('materia.index')
      ->with('success','Materia actualizada satisfactoriamente');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      materia::destroy($id);

      return redirect()->route('materia.index')
                      ->with('success','Materia eliminada satisfactoriamente');
    }
}
