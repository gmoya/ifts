<?php

namespace App\Http\Controllers;

use App\Models\Docente;
use App\Models\EstadoCivil;
use App\Models\Nacionalidad;
use App\Models\Persona;
use App\Models\TipoDocumento;
use App\Models\TipoSexo;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Docente::paginate(20);
    
        return view('docente.index',compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $tipos_doc = TipoDocumento::all();
        $paises = Nacionalidad::all();
        $sexos = TipoSexo::all();
        $estados_civil = EstadoCivil::all();
        
        return view('docente.create', compact('tipos_doc',  'paises',  'sexos',  'estados_civil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // @todo - agregar validadores
        $data = $request->all();
        $fechanac = $data['persona']['fecha_nacimiento'];
        $data['persona']['fecha_nacimiento'] = Carbon::createFromFormat('d/m/Y', $fechanac)->format('Y-m-d');

        $persona = Persona::create($data['persona']);
        $data['persona']['persona_id'] = $persona->id;
        
        Docente::create($data['persona']);

        return redirect()->route('docente.index')
                    ->with('success','Docente creado satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        $docente = Docente::findOrfail($id);
        
        return view('docente.show',compact('docente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $docente = Docente::findOrfail($id);
        $tipos_doc = TipoDocumento::all();
        $paises = Nacionalidad::all();
        $sexos = TipoSexo::all();
        $estados_civil = EstadoCivil::all();
        
        return view('docente.edit', compact('docente', 'tipos_doc',  'paises',  'sexos',  'estados_civil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $docente = Docente::findOrfail($id);

        $data = request()->except(['_token','_method']);
        Persona::where('id', '=' , $docente->persona->id)->update($data['persona']);

        
        return redirect()->route('docente.index')
            ->with('success','Docente actualizada satisfactoriamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      docente::destroy($id);

      return redirect()->route('docente.index')
                      ->with('success','Docente eliminada satisfactoriamente');
    }
}
