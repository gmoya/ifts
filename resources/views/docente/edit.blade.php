@extends('layout')
   
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <br>
            <h2>Editar Materia</h2>
        </div>
        <div class="pull-right">
            <br>
            <a class="btn btn-primary" href="{{ route('docente.index') }}"> Atrás</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Hay algunos problemas con lo que ingresaste.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('docente.update',$docente->id) }}" method="POST">
    @csrf
    {{@method_field('PATCH')}}
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <input type="text" name="persona[nombre]" class="form-control" value="{{ $docente->persona->nombre }}" placeholder="Ingresar nombre">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <strong>Apellido:</strong>
              <input type="text" name="persona[apellido]" class="form-control" value="{{ $docente->persona->apellido }}" placeholder="Ingresar apellido">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo Documento:</strong>
                <select class="form-control" name="persona[tipo_documento_id]">
                    <option value="">---Seleccione---</option>
                    @foreach ($tipos_doc as $tipo)
                        @if ($tipo->id == $docente->persona->tipo_documento_id)
                        <option selected="selected" value="{{ $tipo->id }}">{{ $tipo->descripcion }}</option>
                        @else
                        <option value="{{ $tipo->id }}">{{ $tipo->descripcion }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
      </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nro. Documento:</strong>
                <input type="number" name="persona[nro_documento]" class="form-control" value="{{ $docente->persona->nro_documento }}" placeholder="Ingresar nro. documento">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>País de origen:</strong>
                <select class="form-control" name="persona[nacionalidad_id]">
                    <option value="">---Seleccione---</option>
                    @foreach ($paises as $pais)
                        @if ($pais->id == $docente->persona->nacionalidad_id)
                        <option selected="selected" value="{{ $pais->id }}">{{ $pais->nombre }}</option>
                        @else
                        <option value="{{ $pais->id }}">{{ $pais->nombre }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Sexo:</strong>
                <select class="form-control" name="persona[sexo_id]">
                    <option value="">---Seleccione---</option>
                    @foreach ($sexos as $sexo)
                        @if ($sexo->id == $docente->persona->sexo_id)
                        <option selected="selected" value="{{ $sexo->id }}">{{ $sexo->nombre }}</option>
                        @else
                        <option value="{{ $sexo->id }}">{{ $sexo->nombre }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Estado Civil:</strong>
                <select class="form-control" name="persona[estado_civil_id]">
                    <option value="">---Seleccione---</option>
                    @foreach ($estados_civil as $estado)
                        @if ($estado->id == $docente->persona->estado_civil_id)
                        <option selected="selected" value="{{ $estado->id }}">{{ $estado->descripcion }}</option>
                        @else
                        <option value="{{ $estado->id }}">{{ $estado->descripcion }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Fecha de Nacimiento:</strong>
                <input type="text" name="persona[fecha_nacimiento]" class="form-control" value="{{ $docente->persona->fecha_nacimiento }}" placeholder="Ingresar fecha de nacimiento" class="form-control datepicker">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Lugar de Nacimiento:</strong>
                <input type="text" name="persona[lugar_nacimiento]" class="form-control" value="{{ $docente->persona->lugar_nacimiento }}" placeholder="Ingresar lugar de nacimiento">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Celular:</strong>
                <input type="text" name="persona[celular]" class="form-control" value="{{ $docente->persona->celular }}" placeholder="Ingresar celular">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="persona[email]" class="form-control" value="{{ $docente->persona->email }}" placeholder="Ingresar email">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Observaciones:</strong>
                <textarea name="persona[observaciones]" class="form-control" placeholder="Ingresar observaciones">{{ $docente->persona->observaciones }}</textarea>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
   
</form>
@endsection