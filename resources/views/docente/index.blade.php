@extends('layout')
 
@section('content')
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Listado de Docentes</h2>
                <br>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('docente.create') }}"> Nueva docente</a>
                <br>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Documento</th>
            <th>Estado</th>
            <th width="280px">Acción</th>
        </tr>
        @foreach ($data as $key => $value)
        <tr>
            <td>{{ $value->id }}</td> <!––PRIMERA COLUMNA––>
            <td>{{ $value->getNombreCompleto() }}</td> <!––SEGUNDA COLUMNA––>
            <td>{{ $value->persona->nro_documento }}</td> <!––SEGUNDA COLUMNA––>
            <td>{{ $value->estado }}</td> 
            <td>
                <form action="{{ route('docente.destroy',$value->id) }}" method="POST">   
                    <a class="btn btn-info" href="{{ route('docente.show',$value->id) }}">Ver</a>    
                    <a class="btn btn-primary" href="{{ route('docente.edit',$value->id) }}">Editar</a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
    {!! $data->links('pagination::bootstrap-4') !!}
@endsection