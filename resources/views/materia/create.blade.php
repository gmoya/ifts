@extends('layout')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <br>
            <h2>Ingresar nueva Materia</h2>
        </div>
        <div class="pull-right">
            <br>
            <a class="btn btn-primary" href="{{ route('materia.index') }}"> Atrás</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong>Hay algunos problemas con lo que ingresaste.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('materia.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Código:</strong>
                <input type="text" name="codigo" class="form-control" placeholder="Ingresar código">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <strong>Nombre:</strong>
              <input type="text" name="nombre" class="form-control" placeholder="Ingresar nombre">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>Descripción:</strong>
              <textarea class="form-control" style="height:150px" name="descripcion" placeholder="Ingresar descripción"></textarea>
          </div>
      </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Año:</strong>
                {{-- <input type="number"  name="anio" min="1" max="3">  --}}
                <input type="number" name="anio" min="1" max="3" class="form-control" placeholder="Ingresar año">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <strong>Comisión:</strong>
              <input type="text" name="comision" class="form-control" placeholder="Ingresar comisión">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <strong>Carrera:</strong>
            <select class="form-control" name="carrera_id">
              <option value="">---Seleccione---</option>
              @foreach ($carreras as $carrera)
              <option value="{{ $carrera->id }}">{{ $carrera->nombre }}</option>
              @endforeach
            </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Docente:</strong>
                <select class="form-control" name="docente_id">
                    <option value="">---Seleccione---</option>
                    @foreach ($docentes as $docente)
                    <option value="{{ $docente->id }}">{{ $docente->getNombreCompleto() }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
   
</form>
@endsection