@extends('layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <br>
                <h2>Editar Materia</h2>
            </div>
            <div class="pull-right">
                <br>
                <a class="btn btn-primary" href="{{ route('materia.index') }}"> Atrás</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Hay algunos problemas con lo que ingresaste.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('materia.update',$materia->id) }}" method="POST">
        @csrf
        {{@method_field('PATCH')}}

   
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <strong>Código:</strong>
              <input type="text" name="codigo" value="{{ $materia->codigo }}" class="form-control" placeholder="codigo">
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <strong>Nombre:</strong>
              <input type="text" name="nombre" value="{{ $materia->nombre }}" class="form-control" placeholder="nombre">
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                  <strong>Descripción:</strong>
                  <input type="text" name="descripcion" value="{{ $materia->descripcion }}" class="form-control" placeholder="descripcion">
                </div>
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Año:</strong>
                <input type="text" name="anio" value="{{ $materia->anio }}" class="form-control" placeholder="anio">
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Comisión:</strong>
                <input type="text" name="comision" value="{{ $materia->comision }}" class="form-control" placeholder="comision">
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Tipo de carrera id:</strong>
                <select class="form-control" name="carrera_id">
                    <option value="">---Seleccione---</option>
                    @foreach ($carreras as $carrera)
                        @if ($carrera->id == $materia->carrera_id)
                        <option selected="selected" value="{{ $carrera->id }}">{{ $carrera->nombre }}</option>
                        @else
                        <option value="{{ $carrera->id }}">{{ $carrera->nombre }}</option>
                        @endif
                    @endforeach
                </select>
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Docente id:</strong>
                <select class="form-control" name="docente_id">
                    <option value="">---Seleccione---</option>
                    @foreach ($docentes as $docente)
                        @if ($docente->id == $materia->docente_id)
                        <option selected="selected" value="{{ $docente->id }}">{{ $docente->getNombreCompleto() }}</option>
                        @else
                        <option value="{{ $docente->id }}">{{ $docente->getNombreCompleto() }}</option>
                        @endif
                    @endforeach
                </select>
              </div>
          </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
   
    </form>
@endsection