@extends('layout')
 
@section('content')
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Listado de Materias</h2>
                <br>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('materia.create') }}"> Nueva materia</a>
                <br>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
           <th>Id</th>
            <th>Código</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Año</th>
            <th>Comisión</th>
            <th>Carrera</th>
            <th>Docente</th>
            <th width="280px">Acción</th>
        </tr>
        @foreach ($data as $key => $value)
        <tr>
            <td>{{ $value->id }}</td> <!––PRIMERA COLUMNA––>
            <td>{{ $value->codigo }}</td> <!––SEGUNDA COLUMNA––>
            <td>{{ $value->nombre }}</td> <!––SEGUNDA COLUMNA––>
            <td>{{ ($value->descripcion) }}</td>
            <td>{{ $value->anio }}</td> 
            <td>{{ $value->comision }}</td> 
            <td>{{ $value->carrera->nombre }}</td> 
            <td>{{ $value->docente ? $value->docente->getNombreCompleto() : '' }}</td> 
            <td>
                <form action="{{ route('materia.destroy',$value->id) }}" method="POST">   
                    <a class="btn btn-info" href="{{ route('materia.show',$value->id) }}">Ver</a>    
                    <a class="btn btn-primary" href="{{ route('materia.edit',$value->id) }}">Editar</a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
    {!! $data->links('pagination::bootstrap-4') !!}
@endsection