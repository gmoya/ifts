@extends('layout')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <br>
                <h2> Ver Materia</h2>
            </div>
            <div class="pull-right">
                <br>
                <a class="btn btn-primary" href="{{ route ('materia.index') }}"> Atrás</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Código:</strong>
                {{ $materia->codigo}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <strong>Nombre:</strong>
              {{ $materia->nombre }}  </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>Descripción:</strong>
              {{ $materia->descripcion }}          </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Año:</strong>
          {{ $materia->anio }}
          </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Comisión:</strong>
       {{ $materia->comision }}
        </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Carrera:</strong>
      {{ $materia->carrera->nombre }}
      </div>
</div><div class="col-xs-12 col-sm-12 col-md-12">
  <div class="form-group">
    <strong>Docente:</strong>
   {{ $materia->docente->getNombreCompleto() }}
      </div>
</div>
    </div>
@endsection