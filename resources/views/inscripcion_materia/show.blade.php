@extends('layout')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <br>
                <h2> Ver inscripción</h2>
            </div>
            <div class="pull-right">
                <br>
                <a class="btn btn-primary" href="{{ route('inscripcion_materia.index') }}"> Atrás</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Materia:</strong>
                {{ $inscripcion_materia->materia_id }}
            </div>
        </div>
       
        <div class="row">
          
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alumno:</strong>
               {{ $inscripcion_materia->alumno_id }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Carrera:</strong>
               {{ $inscripcion_materia->carrera_id }}
            </div>
        </div>
        @endsection