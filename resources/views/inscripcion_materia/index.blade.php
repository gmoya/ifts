@extends('layout')
 
@section('content')
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Inscripción a Materias</h2>
                <br>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('inscripcion_materia.create') }}"> Nueva inscripción</a>
                <br>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Materia</th>
            <th>Alumno</th>
            <th>Carrera</th>
            
            
            <th width="280px">Acción</th>
        </tr>
        @foreach ($data as $key => $value)
        <tr>
            <td>{{ $value->id }}</td> <!––PRIMERA COLUMNA––>
            <td>{{ $value->materia_id }}</td> <!––SEGUNDA COLUMNA––>
            <td>{{ $value->alumno_id }}</td> 
            <td>{{ $value->carrera_id }}</td> 
            
            <td>
                <form action="{{ route('inscripcion_materia.destroy',$value->id) }}" method="POST">   
                    <a class="btn btn-info" href="{{ route('inscripcion_materia.show',$value->id) }}">Ver</a>    
                    <a class="btn btn-primary" href="{{ route('inscripcion_materia.edit',$value->id) }}">Editar</a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
    {!! $data->links('pagination::bootstrap-4') !!}
@endsection