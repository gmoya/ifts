@extends('layout')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <br>
            <h2>Ingresar Nueva Inscripción</h2>
        </div>
        <div class="pull-right">
            <br>
            <a class="btn btn-primary" href="{{ route('inscripcion_materia.index') }}"> Atrás</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Hay algunos problemas con lo que ingresaste.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('inscripcion_materia.store') }}" method="POST">
    @csrf
  
    {{-- <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12"> --}}
          {{-- <div class="form-group">
                <strong>Materia:</strong>
                <input type="text" name="materia_id" class="form-control" placeholder="Ingresar materia">

            </div>
        </div>  --}}
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alumno:</strong>
                <input type="text" name="alumno_id" class="form-control" placeholder="Ingresar alumno">
            </div>
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>Materia:</strong>
              <input type="text" name="materia_id" class="form-control" placeholder="Ingresar materia">
          </div>
      </div> 
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Carrera:</strong>
            <input type="text" name="carrera_id" class="form-control" placeholder="Ingresar carrera">
        </div>
    </div> 


        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label >Materia</label>
            <select class="form-control" name="id">
              @foreach ($materia as $materia)
              <option value="{{$materia->id}}">{{$materia->id}}</option>
                  
              @endforeach
            </select> --}}
        
        
        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label >Carrera</label>
            <select class="form-control" name="id">
              @foreach ($carrera as $carrera)
              <option value="{{$carrera->id}}">{{$carrera->id}}</option>
                  
              @endforeach
            </select>
        --}}
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
   
</form>
@endsection