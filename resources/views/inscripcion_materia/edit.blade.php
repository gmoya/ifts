@extends('layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <br>
                <h2>Editar inscripción</h2>
            </div>
            <div class="pull-right">
                <br>
                <a class="btn btn-primary" href="{{ route('inscripcion_materia.index') }}"> Atrás</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Hay algunos problemas con lo que ingresaste.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('inscripcion_materia.update',$inscripcion_materia->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Materia:</strong>
                    <input type="text" name="materia_id" value="{{ $inscripcion_materia->materia_id }}" class="form-control" placeholder="estado">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Alumno:</strong>
                    <input type="text" name="alumno_id" value="{{ $inscripcion_materia->alumno_id }}" class="form-control" placeholder="alumno_id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Carrera:</strong>
                    <input type="text" name="carrera_id" value="{{ $inscripcion_materia->carrera_id }}" class="form-control" placeholder="carrera_id">
                </div>
            </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Creado por:</strong>
                    <input type="text" name="created_by_id" value="{{ $inscripcion_materia->created_by_id  }}" class="form-control" placeholder="institucion">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Modificado por:</strong>
                    <input type="text" name="updated_by_id" value="{{ $inscripcion_materia->updated_by_id }}" class="form-control" placeholder="Title">
                </div>
            </div> --}}
            
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
   
    </form>
@endsection