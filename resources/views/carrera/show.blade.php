@extends('layout')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <br>
                <h2> Ver Carrera</h2>
            </div>
            <div class="pull-right">
                <br>
                <a class="btn btn-primary" href="{{ route('carrera.index') }}"> Atrás</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $carrera->nombre }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripción:</strong>
                {{ $carrera->descripcion }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Estado:</strong>
               {{ $carrera->estado }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Institución:</strong>
               {{ $carrera->institucion }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>Observaciones:</strong>
              {{ $carrera->observaciones }}
          </div>
      </div>
        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>Observaciones:</strong>
              <input type="text" name="observaciones" value="{{ $carrera->observaciones }}" class="form-control" placeholder="observaciones">
          </div>
      </div> --}}
        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <input type="text" name="nombre" value="{{ $carrera->nombre }}" class="form-control" placeholder="Title">
            </div>
        </div> --}}
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo de carrera id:</strong>
                {{ $carrera->tipo_carrera_id }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo de formación id:</strong>
                {{ $carrera->tipo_formacion_id }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Modalidad dictado id:</strong>
                {{ $carrera->modalidad_dictado_id }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Condición de ingreso id:</strong>
                {{ $carrera->condicion_ingreso_id }}
            </div>
        </div>
    </div>
@endsection