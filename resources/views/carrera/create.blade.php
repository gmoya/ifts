@extends('layout')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <br>
            <h2>Ingresar nueva Carrera</h2>
        </div>
        <div class="pull-right">
            <br>
            <a class="btn btn-primary" href="{{ route('carrera.index') }}"> Atrás</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Hay algunos problemas con lo que ingresaste.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('carrera.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <input type="text" name="nombre" class="form-control" placeholder="Ingresar nombre">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripción:</strong>
                <textarea class="form-control" style="height:150px" name="descripcion" placeholder="Ingresar descripción"></textarea>
            </div>
        </div>
        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Estado:</strong>
                <input type="text" name="estado" class="form-control" placeholder="Ingresar Estado:">
            </div>
        </div> --}}
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label >Estado</label>
            <select class="form-control" name="estado">
              @foreach ($carrera as $carrera)
              <option value="{{$carrera->estado}}">{{$carrera->estado}}</option>
                  
              @endforeach
            </select>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Institución:</strong>
                <input type="text" name="institucion" class="form-control" placeholder="Ingresar institución">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Observaciones:</strong>
                <textarea class="form-control" style="height:150px" name="observaciones" placeholder="Ingresar observación"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo de carrera id:</strong>
                <input type="numb" name="tipo_carrera_id" class="form-control" placeholder="Ingresar tipo de carrera id">
            </div>
        </div>
       
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo de Formación:</strong>
                <input type="numb" name="tipo_formacion_id" class="form-control" placeholder="Ingresar id">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Modalidad dictado id:</strong>
                <input type="numb" name="modalidad_dictado_id" class="form-control" placeholder="Ingresar modalidad dictado id">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Condicion ingreso id:</strong>
                <input type="numb" name="condicion_ingreso_id" class="form-control" placeholder="Ingresar condición ingreso id">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
   
</form>
@endsection