@extends('layout')
 
@section('content')
    <div class="row" style="margin-top: 5rem;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Listado de Carreras</h2>
                <br>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('carrera.create') }}"> Nueva Carrera</a>
                <br>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estado</th>
            <th>Institución</th>
            <th>Observaciones</th>
            <th>Tipo de Carrera id</th>
            <th>Tipo de Formación id</th>
            <th>Modalidad de dictado id</th>
            <th>Condición ingreso id</th>
            <th width="280px">Acción</th>
        </tr>
        @foreach ($data as $key => $value)
        <tr>
            <td>{{ $value->id }}</td> <!––PRIMERA COLUMNA––>
            <td>{{ $value->nombre }}</td> <!––SEGUNDA COLUMNA––>
            <td>{{ \Str::limit($value->descripcion, 100) }}</td>
            <td>{{ $value->estado }}</td> 
            <td>{{ $value->institucion }}</td> 
            <td>{{ \Str::limit($value->observaciones, 100) }}</td>
            <td>{{ $value->tipo_carrera_id }}</td> 
            <td>{{ $value->tipo_formacion_id }}</td> 
            <td>{{ $value->modalidad_dictado_id }}</td>
            <td>{{ $value->condicion_ingreso_id }}</td> 
            <td>
                <form action="{{ route('carrera.destroy',$value->id) }}" method="POST">   
                    <a class="btn btn-info" href="{{ route('carrera.show',$value->id) }}">Ver</a>    
                    <a class="btn btn-primary" href="{{ route('carrera.edit',$value->id) }}">Editar</a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
    {!! $data->links('pagination::bootstrap-4') !!}
@endsection