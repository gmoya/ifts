@extends('layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <br>
                <h2>Editar Carrera</h2>
            </div>
            <div class="pull-right">
                <br>
                <a class="btn btn-primary" href="{{ route('carrera.index') }}"> Atrás</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Hay algunos problemas con lo que ingresaste.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('carrera.update',$carrera->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nombre:</strong>
                    <input type="text" name="nombre" value="{{ $carrera->nombre }}" class="form-control" placeholder="nombre">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Descripción:</strong>
                    <textarea class="form-control" style="height:150px" name="descripcion" placeholder="descripcion">{{ $carrera->descripcion }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Estado:</strong>
                    <input type="text" name="estado" value="{{ $carrera->estado }}" class="form-control" placeholder="estado">
                </div>
            </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <label >Estado</label>
                <select class="form-control" name="estado">
                  @foreach ($carrera as $carrera)
                  <option value="{{$carrera->estado}}"></option>
                      
                  @endforeach
                </select> --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Institución:</strong>
                    <input type="text" name="institucion" value="{{ $carrera->institucion }}" class="form-control" placeholder="institucion">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                  <strong>Observaciones:</strong>
                  <input type="text" name="observaciones" value="{{ $carrera->observaciones }}" class="form-control" placeholder="observaciones">
              </div>
          </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nombre:</strong>
                    <input type="text" name="nombre" value="{{ $carrera->nombre }}" class="form-control" placeholder="Title">
                </div>
            </div> --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tipo de carrera id:</strong>
                    <input type="text" name="tipo_carrera_id" value="{{ $carrera->tipo_carrera_id }}" class="form-control" placeholder="Tipo de carrera id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tipo de formacion id:</strong>
                    <input type="text" name="tipo_formacion_id" value="{{ $carrera->tipo_formacion_id }}" class="form-control" placeholder="Tipo de formacion id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Modalidad dictado id:</strong>
                    <input type="text" name="modalidad_dictado_id" value="{{ $carrera->modalidad_dictado_id }}" class="form-control" placeholder="Modalidad dictado id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Condición de ingreso id:</strong>
                    <input type="text" name="condicion_ingreso_id" value="{{ $carrera->condicion_ingreso_id }}" class="form-control" placeholder="Condición de ingreso id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
   
    </form>
@endsection